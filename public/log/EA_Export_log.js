ToonLog([
{"Level": "INFO", "Message": "Open EA repository..."},
{"Level": "INFO", "Message": "Bepaal de te exporteren inhoud..."},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Informatiemodel Officiele Publicaties&#x27; ({D7FEF211-FB26-4692-8045-FDF1A8F4D28A})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Legenda van de diagrammen&#x27; ({9E187217-A693-4585-93B4-BC483B77322B})"},
{"Level": "TRACE", "Message": "Element {656B46DF-5F45-4524-A750-B0FF90EC8D73} onzichtbaar wegens Display requirement"},
{"Level": "TRACE", "Message": "Element {966F0C82-AF12-423d-9890-15D01A89E75C} onzichtbaar wegens Display requirement"},
{"Level": "TRACE", "Message": "Element {93D2813F-2A72-4ee9-889A-B1B18BFE025F} onzichtbaar wegens Display requirement"},
{"Level": "TRACE", "Message": "Element {8606EAD9-3E16-4123-B143-67DF65BA0D40} onzichtbaar wegens Display requirement"},
{"Level": "TRACE", "Message": "Element {E91A9EB7-A496-4e50-8835-61944694ACA1} onzichtbaar wegens Display requirement"},
{"Level": "TRACE", "Message": "Element {0E25CA49-D74E-48a7-A32E-71C122BBA437} onzichtbaar wegens Display requirement"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Ondersteunde processen&#x27; ({D86310E8-3C19-4a96-8141-943A747F6D46})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Besluiten en bekendmaken&#x27; ({934E83F2-A8CD-4cc9-AECD-E383764770CB})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Beschikbaarstellen&#x27; ({3F5A545E-7EFD-4e6e-9138-3AA71EC20A4D})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Instrument en instrumentversies&#x27; ({1A45BBE5-EC4F-4bb2-BBD7-F5E701CE0479})"},
{"Level": "TRACE", "Message": "Element {15EADFDF-8878-4283-85AE-99CE304ADF00} wordt wegens Display requirement getoond als: {0EF0F41E-D57B-42ab-ADCC-1EB5D5DC82D7}"},
{"Level": "TRACE", "Message": "Element {E30E34EA-F702-49fd-AF81-AE05996432DA} wordt wegens Display requirement getoond als: {5CE37AD3-3BBB-4307-B598-3F00E49E985B}"},
{"Level": "TRACE", "Message": "Element {9618CD38-918A-4194-B9D6-573A3691B8B9} wordt wegens Display requirement getoond als: {5CE37AD3-3BBB-4307-B598-3F00E49E985B}"},
{"Level": "TRACE", "Message": "Element {8B9A2940-6292-4ec8-A712-598A3AC421F4} wordt wegens Display requirement getoond als: {5CE37AD3-3BBB-4307-B598-3F00E49E985B}"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Het FRBR model&#x27; ({188A75C6-1FB5-4d10-8E76-59F3FE5DE8F3})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Identificatie&#x27; ({BD0B9B5F-34C5-48ee-B092-538800D54713})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Modules en revisies&#x27; ({0981F858-5C55-46bf-8A3C-B290EB96923D})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Voorbeeld alias en revisie&#x27; ({E1CE8DCC-0981-4cc3-92D2-F82D73EA4F3D})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Wet- en regelgeving&#x27; ({A1BBB86A-635B-4bf6-B2FC-ECD1CFE6A3C9})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Ontwerpbesluitversie&#x27; ({536ABD5A-3255-4df7-975F-845B2EB6CA5F})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Besluitversie&#x27; ({587A8F51-46CD-44d9-AB6E-A1BEE8573352})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Metadata&#x27; ({E071E430-9238-4cd3-94D9-42ECD77D959F})"},
{"Level": "TRACE", "Message": "Element {EEEE3135-0D10-4498-A131-CD096223F600} wordt wegens Display requirement getoond als: {5CE37AD3-3BBB-4307-B598-3F00E49E985B}"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Consoldatie-informatie&#x27; ({5E740899-BABE-47d1-B620-53A402064B33})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Regelingversie&#x27; ({212F1874-134F-41cc-9EFE-006ABEA7DC38})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Metadata&#x27; ({93CB8E59-0C2B-4382-9E3E-007614134DE4})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Informatieobjectversie&#x27; ({ED1B61D3-297C-4ae6-BD9A-4FDB6BDF83F9})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Metadata&#x27; ({EC4DE25B-FB13-41e6-B632-337DAF9C2791})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Juridische borging van regelgeving&#x27; ({5ED09C2A-D4BE-438e-95AF-F510773407C0})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Samenhang regelgeving&#x27; ({9A5457F2-1E43-4ea9-98A2-1F3705924356})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Offici\u00eble publicaties&#x27; ({E0EFE05F-9FBB-45c6-AB24-93B9FF9037EC})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Offici\u00eble-publicatieversie&#x27; ({E8EDA73E-F3B8-42a9-AB27-E3B3E23B1470})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Bekendmaking van besluit&#x27; ({9E3070BB-712E-49ab-A6B1-2A4A13ACACFC})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Inhoud van instrumentversies&#x27; ({607F1633-975F-4d79-908A-F9C21AF017B9})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Objectgericht modelleren en muteren&#x27; ({61016E98-3B97-4885-A6CE-F61D9C34F3C3})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Objectgericht modelleren&#x27; ({C966BB1F-F808-41f6-A587-C16FB89171B2})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Objectgericht muteren&#x27; ({1F41E8E7-749D-4f44-A087-3F2F6C3E7B91})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Historie van een object&#x27; ({0C48F87B-2414-41c1-85C5-10F3BC23A871})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Tekst&#x27; ({1C062261-12F4-4e0a-B2AE-68BD2C05E44C})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Algemeen&#x27; ({215C5F78-18F8-4a22-85ED-BC1EFD681020})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Regeling&#x27; ({59808F11-4807-4ac3-A9A3-83482D860909})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Besluit&#x27; ({A457B7E8-123F-4da3-8C68-98D8A577F50D})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Niet-STOP gecodeerde tekst&#x27; ({A80657EB-D24B-40c0-9897-6CBB1966BB50})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Geo-informatie&#x27; ({FCCD39A3-C851-4b4b-BC19-F00E0DFFE86D})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Geo-informatieobject&#x27; ({4B8D0B2D-93B7-4306-9F92-7FD0F26A99DE})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Geo-informatieobject mutatie&#x27; ({2DDB0306-87C7-4b90-81D3-E519FD405702})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Consolidatie&#x27; ({81311E5C-8AD3-4b35-B153-B21B50080288})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Consolidatie van een instrument&#x27; ({AFAF97EA-2512-47c9-B2CC-7B9922384BF8})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Besluitvorming&#x27; ({6BF47D2B-E072-4e29-9BFE-3369A0A04DEC})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Toestand&#x27; ({CB12418C-709E-4817-8B14-F8F0C877ED59})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Geldigheid&#x27; ({F150908D-2094-449c-9308-177E9A4D8D2F})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Voorbeeld geldigheidsperiode&#x27; ({DB8AC853-8356-4c92-9125-F6D1E159CCC8})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Voorbeeld instrument-stamversie&#x27; ({D1241BE4-867E-4bdf-95F5-A94AD21345C0})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Voorbeeld Samenloop-stamversies&#x27; ({52F3C4DB-8747-472e-AD6C-71ECC8BBAFC9})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Voorbeeld ontbrekende inhoud&#x27; ({82DA523F-2FE5-4057-953C-CED31A85548C})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Geldige regelgeving&#x27; ({24A57166-B3ED-4fbc-AE1C-05209AF2E460})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Juridisch borgen van plannen&#x27; ({15FA5150-3E54-4794-8D3A-DC6FF42AD79E})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Voorbeeld&#x27; ({0A905B5B-82F5-4e5b-97F1-2A61F4B437D5})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Samenhang met toestanden&#x27; ({C2BA6898-0D7C-4303-80A9-706FEA84619C})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Begrippen en waardelijsten&#x27; ({E15D6EFB-90DE-4277-A46E-39F7FE5EFFCC})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;STOP en linked data&#x27; ({C0DF21A3-FC7A-4ff2-A7A7-FEF848B6260F})"},
{"Level": "TRACE", "Message": "Onderzoek package &#x27;Waardelijsten&#x27; ({B23DD48A-67B6-42f6-9DDB-45E1663593C4})"},
{"Level": "INFO", "Message": "Maak DITA bestanden aan..."},
{"Level": "TRACE", "Message": "Maak bestanden voor linkbare objecten"},
{"Level": "TRACE", "Message": "Documentatiebestand hernoemd: &#x27;C:\\Git\\KOOP\\STOP\\ontwikkeling\\documentatie\\STOP_0.98-kern\\uitgeleverd\\informatiemodel\\Informatiemodel_Officiele_Publicaties_(onderwerp)-D7FEF211FB2646928045FDF1A8F4D28A.xml&#x27; =&gt; &#x27;C:\\Git\\KOOP\\STOP\\ontwikkeling\\documentatie\\STOP_0.98-kern\\uitgeleverd\\informatiemodel\\Informatiemodel_Officiele_Publicaties_(onderwerp)-D7FEF211FB2646928045FDF1A8F4D28A.xml&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA map"},
{"Level": "TRACE", "Message": "Maak index topic"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {9E187217-A693-4585-93B4-BC483B77322B} &#x27;Legenda van de diagrammen&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {934E83F2-A8CD-4cc9-AECD-E383764770CB} &#x27;Besluiten en bekendmaken&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {3F5A545E-7EFD-4e6e-9138-3AA71EC20A4D} &#x27;Beschikbaarstellen&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {D86310E8-3C19-4a96-8141-943A747F6D46} &#x27;Ondersteunde processen&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {188A75C6-1FB5-4d10-8E76-59F3FE5DE8F3} &#x27;Het FRBR model&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {BD0B9B5F-34C5-48ee-B092-538800D54713} &#x27;Identificatie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {E1CE8DCC-0981-4cc3-92D2-F82D73EA4F3D} &#x27;Voorbeeld alias en revisie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {0981F858-5C55-46bf-8A3C-B290EB96923D} &#x27;Modules en revisies&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {536ABD5A-3255-4df7-975F-845B2EB6CA5F} &#x27;Ontwerpbesluitversie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {E071E430-9238-4cd3-94D9-42ECD77D959F} &#x27;Metadata&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {5E740899-BABE-47d1-B620-53A402064B33} &#x27;Consoldatie-informatie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {587A8F51-46CD-44d9-AB6E-A1BEE8573352} &#x27;Besluitversie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {93CB8E59-0C2B-4382-9E3E-007614134DE4} &#x27;Metadata&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {212F1874-134F-41cc-9EFE-006ABEA7DC38} &#x27;Regelingversie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {EC4DE25B-FB13-41e6-B632-337DAF9C2791} &#x27;Metadata&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {ED1B61D3-297C-4ae6-BD9A-4FDB6BDF83F9} &#x27;Informatieobjectversie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {5ED09C2A-D4BE-438e-95AF-F510773407C0} &#x27;Juridische borging van regelgeving&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {9A5457F2-1E43-4ea9-98A2-1F3705924356} &#x27;Samenhang regelgeving&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {A1BBB86A-635B-4bf6-B2FC-ECD1CFE6A3C9} &#x27;Wet- en regelgeving&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {9E3070BB-712E-49ab-A6B1-2A4A13ACACFC} &#x27;Bekendmaking van besluit&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {E8EDA73E-F3B8-42a9-AB27-E3B3E23B1470} &#x27;Offici\u00eble-publicatieversie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {E0EFE05F-9FBB-45c6-AB24-93B9FF9037EC} &#x27;Offici\u00eble publicaties&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {1A45BBE5-EC4F-4bb2-BBD7-F5E701CE0479} &#x27;Instrument en instrumentversies&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {C966BB1F-F808-41f6-A587-C16FB89171B2} &#x27;Objectgericht modelleren&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {1F41E8E7-749D-4f44-A087-3F2F6C3E7B91} &#x27;Objectgericht muteren&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {0C48F87B-2414-41c1-85C5-10F3BC23A871} &#x27;Historie van een object&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {61016E98-3B97-4885-A6CE-F61D9C34F3C3} &#x27;Objectgericht modelleren en muteren&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {215C5F78-18F8-4a22-85ED-BC1EFD681020} &#x27;Algemeen&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {59808F11-4807-4ac3-A9A3-83482D860909} &#x27;Regeling&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {A457B7E8-123F-4da3-8C68-98D8A577F50D} &#x27;Besluit&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {A80657EB-D24B-40c0-9897-6CBB1966BB50} &#x27;Niet-STOP gecodeerde tekst&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {1C062261-12F4-4e0a-B2AE-68BD2C05E44C} &#x27;Tekst&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {4B8D0B2D-93B7-4306-9F92-7FD0F26A99DE} &#x27;Geo-informatieobject&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {2DDB0306-87C7-4b90-81D3-E519FD405702} &#x27;Geo-informatieobject mutatie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {FCCD39A3-C851-4b4b-BC19-F00E0DFFE86D} &#x27;Geo-informatie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {607F1633-975F-4d79-908A-F9C21AF017B9} &#x27;Inhoud van instrumentversies&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {AFAF97EA-2512-47c9-B2CC-7B9922384BF8} &#x27;Consolidatie van een instrument&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {6BF47D2B-E072-4e29-9BFE-3369A0A04DEC} &#x27;Besluitvorming&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {DB8AC853-8356-4c92-9125-F6D1E159CCC8} &#x27;Voorbeeld geldigheidsperiode&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {D1241BE4-867E-4bdf-95F5-A94AD21345C0} &#x27;Voorbeeld instrument-stamversie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {52F3C4DB-8747-472e-AD6C-71ECC8BBAFC9} &#x27;Voorbeeld Samenloop-stamversies&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {82DA523F-2FE5-4057-953C-CED31A85548C} &#x27;Voorbeeld ontbrekende inhoud&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {F150908D-2094-449c-9308-177E9A4D8D2F} &#x27;Geldigheid&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {CB12418C-709E-4817-8B14-F8F0C877ED59} &#x27;Toestand&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {24A57166-B3ED-4fbc-AE1C-05209AF2E460} &#x27;Geldige regelgeving&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {0A905B5B-82F5-4e5b-97F1-2A61F4B437D5} &#x27;Voorbeeld&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {C2BA6898-0D7C-4303-80A9-706FEA84619C} &#x27;Samenhang met toestanden&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {15FA5150-3E54-4794-8D3A-DC6FF42AD79E} &#x27;Juridisch borgen van plannen&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {81311E5C-8AD3-4b35-B153-B21B50080288} &#x27;Consolidatie&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {C0DF21A3-FC7A-4ff2-A7A7-FEF848B6260F} &#x27;STOP en linked data&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {B23DD48A-67B6-42f6-9DDB-45E1663593C4} &#x27;Waardelijsten&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {E15D6EFB-90DE-4277-A46E-39F7FE5EFFCC} &#x27;Begrippen en waardelijsten&#x27;"},
{"Level": "TRACE", "Message": "Maak DITA topic voor package {D7FEF211-FB26-4692-8045-FDF1A8F4D28A} &#x27;Informatiemodel Officiele Publicaties&#x27;"}],
{"Taak": "EA export", "Start": "05-11-2019 14:08:27", "Door": "Theun Fleer", "Errors": 0, "Warnings": 0, "Log": "EA_Export_log.js"});